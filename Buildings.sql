-- === Buildings ===

-- Building Costs (Crystalline Cat)

-- City Center
-- Granary unchanged
UPDATE Buildings SET Cost =  60 WHERE BuildingType='BUILDING_WALLS'; -- Ancient Walls, Reduced from 80
UPDATE Buildings SET Cost = 150 WHERE BuildingType='BUILDING_CASTLE'; -- Medieval Walls, Reduced from 225
UPDATE Buildings SET Cost = 200 WHERE BuildingType='BUILDING_STAR_FORT'; -- Renaissance Walls, Reduced from 305
-- Sewer cost changed below w/ housing updates, included here for reference
-- UPDATE Buildings SET Cost = 150 WHERE BuildingType='BUILDING_SEWER'; -- Reduced from 200

-- Campus
-- Library Unchanged
UPDATE Buildings SET Cost = 200 WHERE BuildingType='BUILDING_UNIVERSITY'; -- Reduced from 250
UPDATE Buildings SET Cost = 200 WHERE BuildingType='BUILDING_MADRASA'; -- Reduced from 250
UPDATE Buildings SET Cost = 420 WHERE BuildingType='BUILDING_RESEARCH_LAB'; -- Reduced from 580

UPDATE Building_YieldChanges SET YieldChange = 6 WHERE BuildingType = 'BUILDING_RESEARCH_LAB' AND YieldType = 'YIELD_SCIENCE';

-- Commercial Hub
UPDATE Buildings SET Cost = 110 WHERE BuildingType = 'BUILDING_MARKET'; -- Reduced from 120
UPDATE Buildings SET Cost = 110 WHERE BuildingType = 'BUILDING_SUKIENNICE'; -- Reduced from 120
UPDATE Buildings SET Cost = 240 WHERE BuildingType = 'BUILDING_BANK'; -- Reduced from 290
UPDATE Buildings SET Cost = 310 WHERE BuildingType = 'BUILDING_STOCK_EXCHANGE'; -- Reduced from 390

UPDATE Building_YieldChanges SET YieldChange =  6 WHERE BuildingType = 'BUILDING_BANK'           AND YieldType = 'YIELD_GOLD';
UPDATE Building_YieldChanges SET YieldChange = 10 WHERE BuildingType = 'BUILDING_STOCK_EXCHANGE' AND YieldType = 'YIELD_GOLD';

-- Encampment
-- Barracks, Basilikoi Paides unchanged
UPDATE Buildings SET Cost =  90 WHERE BuildingType = 'BUILDING_STABLE'; -- Reduced from 120
UPDATE Buildings SET Cost = 175 WHERE BuildingType = 'BUILDING_ARMORY'; -- Reduced from 195
UPDATE Buildings SET Cost = 350 WHERE BuildingType = 'BUILDING_MILITARY_ACADEMY'; -- Reduced from 390

-- Entertainment Complex
UPDATE Buildings SET Cost = 130 WHERE BuildingType = 'BUILDING_ARENA'; -- Reduced from 150
UPDATE Buildings SET Cost = 115 WHERE BuildingType = 'BUILDING_TLACHTLI'; -- Reduced from 135
UPDATE Buildings SET Cost = 300 WHERE BuildingType = 'BUILDING_ZOO'; -- Reduced from 445
UPDATE Buildings SET Cost = 425 WHERE BuildingType = 'BUILDING_STADIUM'; -- Reduced from 660

UPDATE Buildings SET Entertainment = 2 WHERE BuildingType = 'BUILDING_ARENA';
UPDATE Buildings SET Entertainment = 2 WHERE BuildingType = 'BUILDING_TLACHTLI';

-- Harbor
UPDATE Buildings SET Cost = 110 WHERE BuildingType = 'BUILDING_LIGHTHOUSE'; -- Reduced from 120
UPDATE Buildings SET Cost = 240 WHERE BuildingType = 'BUILDING_SHIPYARD'; -- Reduced from 290
UPDATE Buildings SET Cost = 420 WHERE BuildingType = 'BUILDING_SEAPORT'; -- Reduced from 580

-- Holy Site
-- Shrine, Temple, Stave Church, Prasat unchanged
UPDATE Buildings SET Cost = 175 WHERE Cost = 190 AND EnabledByReligion = 1; -- All cost 190, is that filter necessary?

-- Industrial Zone
UPDATE Buildings SET Cost = 120 WHERE BuildingType = 'BUILDING_WORKSHOP'; -- Reduced from 195
UPDATE Buildings SET Cost = 275 WHERE BuildingType = 'BUILDING_FACTORY'; -- Reduced from 390
UPDATE Buildings SET Cost = 400 WHERE BuildingType = 'BUILDING_POWER_PLANT'; -- Reduced from 580
UPDATE Buildings SET Cost = 400 WHERE BuildingType = 'BUILDING_ELECTRONICS_FACTORY'; -- Reduced from 580

-- Theater Square
UPDATE Buildings SET Cost = 130 WHERE BuildingType = 'BUILDING_AMPHITHEATER'; -- Reduced from 150
UPDATE Buildings SET Cost = 240 WHERE BuildingType = 'BUILDING_MUSEUM_ART'; -- Reduced from 290
UPDATE Buildings SET Cost = 240 WHERE BuildingType = 'BUILDING_MUSEUM_ARTIFACT'; -- Reduced from 290
UPDATE Buildings SET Cost = 420 WHERE BuildingType = 'BUILDING_BROADCAST_CENTER'; -- Reduced from 580
UPDATE Buildings SET Cost = 420 WHERE BuildingType = 'BUILDING_FILM_STUDIO'; -- Reduced from 580

-- Aerodrome
UPDATE Buildings SET Cost = 300 WHERE BuildingType = 'BUILDING_HANGAR'; -- Reduced from 465
UPDATE Buildings SET Cost = 400 WHERE BuildingType = 'BUILDING_AIRPORT'; -- Reduced from 600

-- Changes to granary and sewer housing (Crystalline Cat)
UPDATE Buildings SET Housing = 3                                                    WHERE BuildingType = 'BUILDING_GRANARY';
UPDATE Buildings SET Housing = 3, Cost = 150, PrereqTech = 'TECH_SCIENTIFIC_THEORY' WHERE BuildingType = 'BUILDING_SEWER';

-- Relocate Zoo in Tech Tree to Colonialism (Suboptimal)
UPDATE Buildings SET PrereqCivic = 'CIVIC_COLONIALISM' WHERE BuildingType = 'BUILDING_ZOO';

-- Citizen Slots increased to 2 for all district buildings that currently have slots (Suboptimal)
UPDATE Buildings SET CitizenSlots = 2 WHERE CitizenSlots = 1;

-- Buildings provide +25% boost to corresponding city yields (Suboptimal)

-- Campus Buildings

INSERT INTO BuildingModifiers                           VALUES('BUILDING_LIBRARY', 'LIBRARY_YIELD_MULT');
INSERT INTO Modifiers (ModifierID, ModifierType)        VALUES('LIBRARY_YIELD_MULT', 'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_MODIFIER');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('LIBRARY_YIELD_MULT', 'BuildingType', 'BUILDING_LIBRARY');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('LIBRARY_YIELD_MULT', 'YieldType', 'YIELD_SCIENCE');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('LIBRARY_YIELD_MULT', 'Amount', '25');
UPDATE Buildings SET Description = 'LOC_BUILDING_LIBRARY_DESCRIPTION' WHERE BuildingType = 'BUILDING_LIBRARY';

INSERT INTO BuildingModifiers                           VALUES('BUILDING_UNIVERSITY', 'UNIVERSITY_YIELD_MULT');
INSERT INTO Modifiers (ModifierID, ModifierType)        VALUES('UNIVERSITY_YIELD_MULT', 'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_MODIFIER');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('UNIVERSITY_YIELD_MULT', 'BuildingType', 'BUILDING_UNIVERSITY');
INSERT INTO ModifierArguments (ModifierID, Name, Value)VALUES('UNIVERSITY_YIELD_MULT', 'YieldType', 'YIELD_SCIENCE');
INSERT INTO ModifierArguments (ModifierID, Name, Value)VALUES('UNIVERSITY_YIELD_MULT', 'Amount', '25');
UPDATE Buildings SET Description = 'LOC_BUILDING_UNIVERSITY_DESCRIPTION' WHERE BuildingType = 'BUILDING_UNIVERSITY';

INSERT INTO BuildingModifiers                           VALUES('BUILDING_MADRASA', 'MADRASA_YIELD_MULT');
INSERT INTO Modifiers (ModifierID, ModifierType)        VALUES('MADRASA_YIELD_MULT', 'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_MODIFIER');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('MADRASA_YIELD_MULT', 'BuildingType', 'BUILDING_MADRASA');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('MADRASA_YIELD_MULT', 'YieldType', 'YIELD_SCIENCE');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('MADRASA_YIELD_MULT', 'Amount', '25');

INSERT INTO BuildingModifiers                           VALUES('BUILDING_RESEARCH_LAB', 'RESEARCH_LAB_YIELD_MULT');
INSERT INTO Modifiers (ModifierID, ModifierType)        VALUES('RESEARCH_LAB_YIELD_MULT', 'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_MODIFIER');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('RESEARCH_LAB_YIELD_MULT', 'BuildingType', 'BUILDING_RESEARCH_LAB');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('RESEARCH_LAB_YIELD_MULT', 'YieldType', 'YIELD_SCIENCE');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('RESEARCH_LAB_YIELD_MULT', 'Amount', '25');
UPDATE Buildings SET Description = 'LOC_BUILDING_RESEARCH_LAB_DESCRIPTION' WHERE BuildingType = 'BUILDING_RESEARCH_LAB';

-- Commercial Hub Buildings

INSERT INTO BuildingModifiers                           VALUES('BUILDING_MARKET', 'MARKET_YIELD_MULT');
INSERT INTO Modifiers (ModifierID, ModifierType)        VALUES('MARKET_YIELD_MULT', 'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_MODIFIER');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('MARKET_YIELD_MULT', 'BuildingType', 'BUILDING_MARKET');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('MARKET_YIELD_MULT', 'YieldType', 'YIELD_GOLD');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('MARKET_YIELD_MULT', 'Amount', '25');
UPDATE Buildings SET Description = 'LOC_BUILDING_MARKET_DESCRIPTION' WHERE BuildingType = 'BUILDING_MARKET';

INSERT INTO BuildingModifiers                           VALUES('BUILDING_SUKIENNICE', 'SUKIENNICE_YIELD_MULT');
INSERT INTO Modifiers (ModifierID, ModifierType)        VALUES('SUKIENNICE_YIELD_MULT', 'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_MODIFIER');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('SUKIENNICE_YIELD_MULT', 'BuildingType', 'BUILDING_SUKIENNICE');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('SUKIENNICE_YIELD_MULT', 'YieldType', 'YIELD_GOLD');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('SUKIENNICE_YIELD_MULT', 'Amount', '25');

INSERT INTO BuildingModifiers                           VALUES('BUILDING_BANK', 'BANK_YIELD_MULT');
INSERT INTO Modifiers (ModifierID, ModifierType)        VALUES('BANK_YIELD_MULT', 'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_MODIFIER');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('BANK_YIELD_MULT', 'BuildingType', 'BUILDING_BANK');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('BANK_YIELD_MULT', 'YieldType', 'YIELD_GOLD');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('BANK_YIELD_MULT', 'Amount', '25');
UPDATE Buildings SET Description = 'LOC_BUILDING_BANK_DESCRIPTION' WHERE BuildingType = 'BUILDING_BANK';

INSERT INTO BuildingModifiers                           VALUES('BUILDING_STOCK_EXCHANGE', 'STOCK_EXCHANGE_YIELD_MULT');
INSERT INTO Modifiers (ModifierID, ModifierType)        VALUES('STOCK_EXCHANGE_YIELD_MULT', 'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_MODIFIER');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('STOCK_EXCHANGE_YIELD_MULT', 'BuildingType', 'BUILDING_STOCK_EXCHANGE');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('STOCK_EXCHANGE_YIELD_MULT', 'YieldType', 'YIELD_GOLD');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('STOCK_EXCHANGE_YIELD_MULT', 'Amount', '25');
UPDATE Buildings SET Description = 'LOC_BUILDING_STOCK_EXCHANGE_DESCRIPTION' WHERE BuildingType = 'BUILDING_STOCK_EXCHANGE';

-- Industrial Zone Buildings

INSERT INTO BuildingModifiers                           VALUES('BUILDING_WORKSHOP', 'WORKSHOP_YIELD_MULT');
INSERT INTO Modifiers (ModifierID, ModifierType)        VALUES('WORKSHOP_YIELD_MULT', 'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_MODIFIER');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('WORKSHOP_YIELD_MULT', 'BuildingType', 'BUILDING_WORKSHOP');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('WORKSHOP_YIELD_MULT', 'YieldType', 'YIELD_PRODUCTION');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('WORKSHOP_YIELD_MULT', 'Amount', '25');
UPDATE Buildings SET Description = 'LOC_BUILDING_WORKSHOP_DESCRIPTION' WHERE BuildingType = 'BUILDING_WORKSHOP';

INSERT INTO BuildingModifiers                           VALUES('BUILDING_FACTORY', 'FACTORY_YIELD_MULT');
INSERT INTO Modifiers (ModifierID, ModifierType)        VALUES('FACTORY_YIELD_MULT', 'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_MODIFIER');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('FACTORY_YIELD_MULT', 'BuildingType', 'BUILDING_FACTORY');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('FACTORY_YIELD_MULT', 'YieldType', 'YIELD_PRODUCTION');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('FACTORY_YIELD_MULT', 'Amount', '25');

INSERT INTO BuildingModifiers                           VALUES('BUILDING_ELECTRONICS_FACTORY', 'ELECTRONICS_FACTORY_YIELD_MULT');
INSERT INTO Modifiers (ModifierID, ModifierType)        VALUES('ELECTRONICS_FACTORY_YIELD_MULT', 'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_MODIFIER');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('ELECTRONICS_FACTORY_YIELD_MULT', 'BuildingType', 'BUILDING_ELECTRONICS_FACTORY');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('ELECTRONICS_FACTORY_YIELD_MULT', 'YieldType', 'YIELD_PRODUCTION');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('ELECTRONICS_FACTORY_YIELD_MULT', 'Amount', '25');

INSERT INTO BuildingModifiers                           VALUES('BUILDING_POWER_PLANT', 'POWER_PLANT_YIELD_MULT');
INSERT INTO Modifiers (ModifierID, ModifierType)        VALUES('POWER_PLANT_YIELD_MULT', 'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_MODIFIER');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('POWER_PLANT_YIELD_MULT', 'BuildingType', 'BUILDING_POWER_PLANT');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('POWER_PLANT_YIELD_MULT', 'YieldType', 'YIELD_PRODUCTION');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('POWER_PLANT_YIELD_MULT', 'Amount', '25');

-- Theater Square Buildings

INSERT INTO BuildingModifiers                           VALUES('BUILDING_AMPHITHEATER', 'AMPHITHEATER_YIELD_MULT');
INSERT INTO Modifiers (ModifierID, ModifierType)        VALUES('AMPHITHEATER_YIELD_MULT', 'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_MODIFIER');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('AMPHITHEATER_YIELD_MULT', 'BuildingType', 'BUILDING_AMPHITHEATER');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('AMPHITHEATER_YIELD_MULT', 'YieldType', 'YIELD_CULTURE');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('AMPHITHEATER_YIELD_MULT', 'Amount', '25');
UPDATE Buildings SET Description = 'LOC_BUILDING_AMPHITHEATER_DESCRIPTION' WHERE BuildingType = 'BUILDING_AMPHITHEATER';

INSERT INTO BuildingModifiers                           VALUES('BUILDING_MUSEUM_ART', 'MUSEUM_ART_YIELD_MULT');
INSERT INTO Modifiers (ModifierID, ModifierType)        VALUES('MUSEUM_ART_YIELD_MULT', 'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_MODIFIER');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('MUSEUM_ART_YIELD_MULT', 'BuildingType', 'BUILDING_MUSEUM_ART');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('MUSEUM_ART_YIELD_MULT', 'YieldType', 'YIELD_CULTURE');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('MUSEUM_ART_YIELD_MULT', 'Amount', '25');

INSERT INTO BuildingModifiers                           VALUES('BUILDING_MUSEUM_ARTIFACT', 'MUSEUM_ARTIFACT_YIELD_MULT');
INSERT INTO Modifiers (ModifierID, ModifierType)        VALUES('MUSEUM_ARTIFACT_YIELD_MULT', 'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_MODIFIER');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('MUSEUM_ARTIFACT_YIELD_MULT', 'BuildingType', 'BUILDING_MUSEUM_ARTIFACT');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('MUSEUM_ARTIFACT_YIELD_MULT', 'YieldType', 'YIELD_CULTURE');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('MUSEUM_ARTIFACT_YIELD_MULT', 'Amount', '25');

INSERT INTO BuildingModifiers                           VALUES('BUILDING_BROADCAST_CENTER', 'BROADCAST_CENTER_YIELD_MULT');
INSERT INTO Modifiers (ModifierID, ModifierType)        VALUES('BROADCAST_CENTER_YIELD_MULT', 'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_MODIFIER');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('BROADCAST_CENTER_YIELD_MULT', 'BuildingType', 'BUILDING_BROADCAST_CENTER');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('BROADCAST_CENTER_YIELD_MULT', 'YieldType', 'YIELD_CULTURE');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('BROADCAST_CENTER_YIELD_MULT', 'Amount', '25');
UPDATE Buildings SET Description = 'LOC_BUILDING_BROADCAST_CENTER_DESCRIPTION' WHERE BuildingType = 'BUILDING_BROADCAST_CENTER';

INSERT INTO BuildingModifiers                           VALUES('BUILDING_FILM_STUDIO', 'FILM_STUDIO_YIELD_MULT');
INSERT INTO Modifiers (ModifierID, ModifierType)        VALUES('FILM_STUDIO_YIELD_MULT', 'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_MODIFIER');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('FILM_STUDIO_YIELD_MULT', 'BuildingType', 'BUILDING_FILM_STUDIO');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('FILM_STUDIO_YIELD_MULT', 'YieldType', 'YIELD_CULTURE');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('FILM_STUDIO_YIELD_MULT', 'Amount', '25');

-- Holy Site Buildings

INSERT INTO BuildingModifiers                           VALUES('BUILDING_SHRINE', 'SHRINE_YIELD_MULT');
INSERT INTO Modifiers (ModifierID, ModifierType)        VALUES('SHRINE_YIELD_MULT', 'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_MODIFIER');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('SHRINE_YIELD_MULT', 'BuildingType', 'BUILDING_SHRINE');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('SHRINE_YIELD_MULT', 'YieldType', 'YIELD_FAITH');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('SHRINE_YIELD_MULT', 'Amount', '25');

INSERT INTO BuildingModifiers                           VALUES('BUILDING_TEMPLE', 'TEMPLE_YIELD_MULT');
INSERT INTO Modifiers (ModifierID, ModifierType)        VALUES('TEMPLE_YIELD_MULT', 'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_MODIFIER');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('TEMPLE_YIELD_MULT', 'BuildingType', 'BUILDING_TEMPLE');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('TEMPLE_YIELD_MULT', 'YieldType', 'YIELD_FAITH');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('TEMPLE_YIELD_MULT', 'Amount', '25');

INSERT INTO BuildingModifiers                           VALUES('BUILDING_STAVE_CHURCH', 'STAVE_CHURCH_YIELD_MULT');
INSERT INTO Modifiers (ModifierID, ModifierType)        VALUES('STAVE_CHURCH_YIELD_MULT', 'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_MODIFIER');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('STAVE_CHURCH_YIELD_MULT', 'BuildingType', 'BUILDING_STAVE_CHURCH');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('STAVE_CHURCH_YIELD_MULT', 'YieldType', 'YIELD_FAITH');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('STAVE_CHURCH_YIELD_MULT', 'Amount', '25');

INSERT INTO BuildingModifiers                           VALUES('BUILDING_PRASAT', 'PRASAT_YIELD_MULT');
INSERT INTO Modifiers (ModifierID, ModifierType)        VALUES('PRASAT_YIELD_MULT', 'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_MODIFIER');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('PRASAT_YIELD_MULT', 'BuildingType', 'BUILDING_PRASAT');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('PRASAT_YIELD_MULT', 'YieldType', 'YIELD_FAITH');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('PRASAT_YIELD_MULT', 'Amount', '25');

INSERT INTO BuildingModifiers                           VALUES('BUILDING_CATHEDRAL', 'CATHEDRAL_YIELD_MULT');
INSERT INTO Modifiers (ModifierID, ModifierType)        VALUES('CATHEDRAL_YIELD_MULT', 'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_MODIFIER');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('CATHEDRAL_YIELD_MULT', 'BuildingType', 'BUILDING_CATHEDRAL');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('CATHEDRAL_YIELD_MULT', 'YieldType', 'YIELD_FAITH');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('CATHEDRAL_YIELD_MULT', 'Amount', '25');
UPDATE Buildings SET Description = 'LOC_BUILDING_CATHEDRAL_DESCRIPTION' WHERE BuildingType = 'BUILDING_CATHEDRAL';

INSERT INTO BuildingModifiers                           VALUES('BUILDING_GURDWARA', 'GURDWARA_YIELD_MULT');
INSERT INTO Modifiers (ModifierID, ModifierType)        VALUES('GURDWARA_YIELD_MULT', 'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_MODIFIER');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('GURDWARA_YIELD_MULT', 'BuildingType', 'BUILDING_GURDWARA');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('GURDWARA_YIELD_MULT', 'YieldType', 'YIELD_FAITH');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('GURDWARA_YIELD_MULT', 'Amount', '25');
UPDATE Buildings SET Description = 'LOC_BUILDING_GURDWARA_DESCRIPTION' WHERE BuildingType = 'BUILDING_GURDWARA';

INSERT INTO BuildingModifiers                           VALUES('BUILDING_MEETING_HOUSE', 'MEETING_HOUSE_YIELD_MULT');
INSERT INTO Modifiers (ModifierID, ModifierType)        VALUES('MEETING_HOUSE_YIELD_MULT', 'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_MODIFIER');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('MEETING_HOUSE_YIELD_MULT', 'BuildingType', 'BUILDING_MEETING_HOUSE');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('MEETING_HOUSE_YIELD_MULT', 'YieldType', 'YIELD_FAITH');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('MEETING_HOUSE_YIELD_MULT', 'Amount', '25');
UPDATE Buildings SET Description = 'LOC_BUILDING_MEETING_HOUSE_DESCRIPTION' WHERE BuildingType = 'BUILDING_MEETING_HOUSE';

INSERT INTO BuildingModifiers                           VALUES('BUILDING_MOSQUE', 'MOSQUE_YIELD_MULT');
INSERT INTO Modifiers (ModifierID, ModifierType)        VALUES('MOSQUE_YIELD_MULT', 'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_MODIFIER');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('MOSQUE_YIELD_MULT', 'BuildingType', 'BUILDING_MOSQUE');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('MOSQUE_YIELD_MULT', 'YieldType', 'YIELD_FAITH');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('MOSQUE_YIELD_MULT', 'Amount', '25');

INSERT INTO BuildingModifiers                           VALUES('BUILDING_PAGODA', 'PAGODA_YIELD_MULT');
INSERT INTO Modifiers (ModifierID, ModifierType)        VALUES('PAGODA_YIELD_MULT', 'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_MODIFIER');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('PAGODA_YIELD_MULT', 'BuildingType', 'BUILDING_PAGODA');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('PAGODA_YIELD_MULT', 'YieldType', 'YIELD_FAITH');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('PAGODA_YIELD_MULT', 'Amount', '25');
UPDATE Buildings SET Description = 'LOC_BUILDING_PAGODA_DESCRIPTION' WHERE BuildingType = 'BUILDING_PAGODA';

INSERT INTO BuildingModifiers                           VALUES('BUILDING_SYNAGOGUE', 'SYNAGOGUE_YIELD_MULT');
INSERT INTO Modifiers (ModifierID, ModifierType)        VALUES('SYNAGOGUE_YIELD_MULT', 'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_MODIFIER');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('SYNAGOGUE_YIELD_MULT', 'BuildingType', 'BUILDING_SYNAGOGUE');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('SYNAGOGUE_YIELD_MULT', 'YieldType', 'YIELD_FAITH');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('SYNAGOGUE_YIELD_MULT', 'Amount', '25');
UPDATE Buildings SET Description = 'LOC_BUILDING_SYNAGOGUE_DESCRIPTION' WHERE BuildingType = 'BUILDING_SYNAGOGUE';

INSERT INTO BuildingModifiers                           VALUES('BUILDING_WAT', 'WAT_YIELD_MULT');
INSERT INTO Modifiers (ModifierID, ModifierType)        VALUES('WAT_YIELD_MULT', 'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_MODIFIER');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('WAT_YIELD_MULT', 'BuildingType', 'BUILDING_WAT');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('WAT_YIELD_MULT', 'YieldType', 'YIELD_FAITH');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('WAT_YIELD_MULT', 'Amount', '25');
UPDATE Buildings SET Description = 'LOC_BUILDING_WAT_DESCRIPTION' WHERE BuildingType = 'BUILDING_WAT';

INSERT INTO BuildingModifiers                           VALUES('BUILDING_STUPA', 'STUPA_YIELD_MULT');
INSERT INTO Modifiers (ModifierID, ModifierType)        VALUES('STUPA_YIELD_MULT', 'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_MODIFIER');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('STUPA_YIELD_MULT', 'BuildingType', 'BUILDING_STUPA');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('STUPA_YIELD_MULT', 'YieldType', 'YIELD_FAITH');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('STUPA_YIELD_MULT', 'Amount', '25');
UPDATE Buildings SET Description = 'LOC_BUILDING_STUPA_DESCRIPTION' WHERE BuildingType = 'BUILDING_STUPA';

INSERT INTO BuildingModifiers                           VALUES('BUILDING_DAR_E_MEHR', 'DAR_E_MEHR_YIELD_MULT');
INSERT INTO Modifiers (ModifierID, ModifierType)        VALUES('DAR_E_MEHR_YIELD_MULT', 'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_MODIFIER');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('DAR_E_MEHR_YIELD_MULT', 'BuildingType', 'BUILDING_DAR_E_MEHR');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('DAR_E_MEHR_YIELD_MULT', 'YieldType', 'YIELD_FAITH');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('DAR_E_MEHR_YIELD_MULT', 'Amount', '25');

-- Encampment Buildings - provide reduction to war weariness (Suboptimal)

INSERT INTO BuildingModifiers                           VALUES('BUILDING_BARRACKS', 'BARRACKS_WARWEARINESS');
INSERT INTO Modifiers (ModifierID, ModifierType)        VALUES('BARRACKS_WARWEARINESS', 'MODIFIER_PLAYER_ADJUST_WAR_WEARINESS');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('BARRACKS_WARWEARINESS', 'BuildingType', 'BUILDING_BARRACKS');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('BARRACKS_WARWEARINESS', 'Overall', '1');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('BARRACKS_WARWEARINESS', 'Amount', '-2');

INSERT INTO BuildingModifiers                           VALUES('BUILDING_BASILIKOI_PAIDES', 'BASILIKOI_PAIDES_WARWEARINESS');
INSERT INTO Modifiers (ModifierID, ModifierType)        VALUES('BASILIKOI_PAIDES_WARWEARINESS', 'MODIFIER_PLAYER_ADJUST_WAR_WEARINESS');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('BASILIKOI_PAIDES_WARWEARINESS', 'BuildingType', 'BUILDING_BASILIKOI_PAIDES');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('BASILIKOI_PAIDES_WARWEARINESS', 'Overall', '1');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('BASILIKOI_PAIDES_WARWEARINESS', 'Amount', '-2');

INSERT INTO BuildingModifiers                           VALUES('BUILDING_STABLE', 'STABLE_WARWEARINESS');
INSERT INTO Modifiers (ModifierID, ModifierType)        VALUES('STABLE_WARWEARINESS', 'MODIFIER_PLAYER_ADJUST_WAR_WEARINESS');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('STABLE_WARWEARINESS', 'BuildingType', 'BUILDING_STABLE');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('STABLE_WARWEARINESS', 'Overall', '1');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('STABLE_WARWEARINESS', 'Amount', '-2');

INSERT INTO BuildingModifiers                           VALUES('BUILDING_ARMORY', 'ARMORY_WARWEARINESS');
INSERT INTO Modifiers (ModifierID, ModifierType)        VALUES('ARMORY_WARWEARINESS', 'MODIFIER_PLAYER_ADJUST_WAR_WEARINESS');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('ARMORY_WARWEARINESS', 'BuildingType', 'BUILDING_ARMORY');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('ARMORY_WARWEARINESS', 'Overall', '1');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('ARMORY_WARWEARINESS', 'Amount', '-3');

INSERT INTO BuildingModifiers                           VALUES('BUILDING_MILITARY_ACADEMY', 'MILITARY_ACADEMY_WARWEARINESS');
INSERT INTO Modifiers (ModifierID, ModifierType)        VALUES('MILITARY_ACADEMY_WARWEARINESS', 'MODIFIER_PLAYER_ADJUST_WAR_WEARINESS');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('MILITARY_ACADEMY_WARWEARINESS', 'BuildingType', 'BUILDING_MILITARY_ACADEMY');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('MILITARY_ACADEMY_WARWEARINESS', 'Overall', '1');
INSERT INTO ModifierArguments (ModifierID, Name, Value) VALUES('MILITARY_ACADEMY_WARWEARINESS', 'Amount', '-5');
