-- Tile Improvements (Crystalline Cat)
UPDATE Features SET AddCivic = 'CIVIC_RECORDED_HISTORY' WHERE FeatureType = 'FEATURE_FOREST';
UPDATE Civics SET Description = 'LOC_CIVIC_RECORDED_HISTORY_DESCRIPTION' WHERE CivicType = 'CIVIC_RECORDED_HISTORY';
UPDATE Features SET Description = "LOC_FEATURE_FOREST_DESCRIPTION" WHERE FeatureType = 'FEATURE_FOREST';
UPDATE Features SET Description = "LOC_FEATURE_MARSH_DESCRIPTION" WHERE FeatureType = 'FEATURE_MARSH';
UPDATE Features SET Description = "LOC_FEATURE_JUNGLE_DESCRIPTION" WHERE FeatureType = 'FEATURE_JUNGLE';

UPDATE Improvement_YieldChanges      SET YieldChange = 1            WHERE ImprovementType = 'IMPROVEMENT_CAMP' AND YieldType   = 'YIELD_FOOD';
UPDATE Improvement_YieldChanges      SET YieldChange = 0            WHERE ImprovementType = 'IMPROVEMENT_CAMP' AND YieldType   = 'YIELD_GOLD';
UPDATE Improvement_BonusYieldChanges SET YieldType   = 'YIELD_GOLD' WHERE ImprovementType = 'IMPROVEMENT_CAMP' AND PrereqCivic = 'CIVIC_MERCANTILISM' AND YieldType = 'YIELD_FOOD';

-- Roads (Crystalline Cat)
UPDATE Routes SET SupportsBridges = 1                                   WHERE RouteType = 'ROUTE_ANCIENT_ROAD';
UPDATE Routes SET MovementCost    = 0.75, PrereqEra = 'ERA_MEDIEVAL'    WHERE RouteType = 'ROUTE_MEDIEVAL_ROAD';
UPDATE Routes SET MovementCost    = 0.66, PrereqEra = 'ERA_RENAISSANCE' WHERE RouteType = 'ROUTE_INDUSTRIAL_ROAD';
UPDATE Routes SET                         PrereqEra = 'ERA_INDUSTRIAL'  WHERE RouteType = 'ROUTE_MODERN_ROAD';


/*
-- Changes to Farms (Suboptimal)
-- THIS ENTIRE BLOCK MUST BE UNCOMMENTED FOR IT TO WORK RIGHT
-- Increase Farm Base Yield on Flat Lands from 1 food to 2 food
UPDATE Improvement_YieldChanges SET YieldChange = 2 WHERE ImprovementType = 'IMPROVEMENT_FARM';

-- Create "Wet Farm" for Rice & Floodplains, yield increase is +1 food
INSERT INTO Types (Type, Kind) VALUES ('IMPROVMENT_WET_FARM', 'KIND_IMPROVEMENT');
INSERT INTO Improvements (ImprovementType, Name, Description, PlunderType, PlunderAmount, Housing, Icon)
	VALUES('IMPROVEMENT_WET_FARM', 'LOC_IMPROVEMENT_FARM_NAME', 'LOC_IMPROVEMENT_FARM_DESCRIPTION', 'PLUNDER_HEAL', '50', '1', 'ICON_IMPROVEMENT_FARM');
	
INSERT INTO Improvement_Adjacencies     VALUES('IMPROVEMENT_WET_FARM', 'Farms_MedievalAdjacency');
INSERT INTO Improvement_Adjacencies     VALUES('IMPROVEMENT_WET_FARM', 'Farms_MechanizedAdjacency');
INSERT INTO Improvement_ValidBuildUnits VALUES('IMPROVEMENT_WET_FARM', 'UNIT_BUILDER');
INSERT INTO Improvement_YieldChanges    VALUES('IMPROVEMENT_WET_FARM', 'YIELD_FOOD', '1');

UPDATE Improvement_ValidFeatures  SET ImprovementType = 'IMPROVEMENT_WET_FARM' WHERE ImprovementType = 'IMPROVEMENT_FARM';
UPDATE Improvement_ValidResources SET ImprovementType = 'IMPROVEMENT_WET_FARM' WHERE ResourceType = 'RESOURCE_RICE';
*/

--Increase Farm Housing from 0.5 to 1.0 per farm (Suboptimal)
--UPDATE Improvements SET TilesRequired = '1' WHERE ImprovementType = 'IMPROVEMENT_FARM';
