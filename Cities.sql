-- === Cities === 

-- Housing: Update City Minimums (Suboptimal)
UPDATE GlobalParameters SET Value = '3' WHERE Name = 'CITY_POPULATION_NO_WATER';
UPDATE GlobalParameters SET Value = '4' WHERE Name = 'CITY_POPULATION_COAST';
UPDATE GlobalParameters SET Value = '6' WHERE Name = 'CITY_POPULATION_RIVER_LAKE';

-- Housing (Crystalline Cat)
UPDATE GlobalParameters SET Value =   '0' WHERE Name = 'CITY_HOUSING_LEFT_50PCT_GROWTH';
UPDATE GlobalParameters SET Value =  '-1' WHERE Name = 'CITY_HOUSING_LEFT_25PCT_GROWTH';
UPDATE GlobalParameters SET Value =  '-5' WHERE Name = 'CITY_HOUSING_LEFT_ZERO_GROWTH';


-- Food Requirements (Crystalline Cat, Modified to 0 by Suboptimal)
UPDATE GlobalParameters SET Value = '0' WHERE Name = 'CITY_GROWTH_EXPONENT';

-- Amenities (Crystalline Cat)
UPDATE Resources SET Happiness = 5 WHERE Happiness = 4;
UPDATE Resources SET Happiness = 7 WHERE Happiness = 6;

-- Cultural Tile Expansion (Crystalline Cat, Modified to 1 by Suboptimal)
UPDATE GlobalParameters SET Value = '1.00' WHERE Name = 'CULTURE_COST_LATER_PLOT_EXPONENT';

