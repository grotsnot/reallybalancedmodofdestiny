INSERT OR REPLACE INTO LocalizedText (Language, Tag, Text)
VALUES
-- Venetian Arsenal only applies to the city in which it is built
("en_US", "LOC_BUILDING_VENETIAN_ARSENAL_DESCRIPTION", "Receive a second naval unit each time you train a naval unit in this city. Must be built on a Coast tile that is adjacent to an Industrial Zone district.");