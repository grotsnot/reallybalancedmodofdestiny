-- === Districts ===

-- Specialists generate GPP (Suboptimal)
INSERT INTO District_CitizenGreatPersonPoints VALUES('DISTRICT_HOLY_SITE', 'GREAT_PERSON_CLASS_PROPHET', 1);
INSERT INTO District_CitizenGreatPersonPoints VALUES('DISTRICT_LAVRA', 'GREAT_PERSON_CLASS_PROPHET', 1);
INSERT INTO District_CitizenGreatPersonPoints VALUES('DISTRICT_CAMPUS', 'GREAT_PERSON_CLASS_SCIENTIST', 1);
INSERT INTO District_CitizenGreatPersonPoints VALUES('DISTRICT_ENCAMPMENT', 'GREAT_PERSON_CLASS_GENERAL', 1);
INSERT INTO District_CitizenGreatPersonPoints VALUES('DISTRICT_HARBOR', 'GREAT_PERSON_CLASS_ADMIRAL', 1);
INSERT INTO District_CitizenGreatPersonPoints VALUES('DISTRICT_ROYAL_NAVY_DOCKYARD', 'GREAT_PERSON_CLASS_ADMIRAL', 1);
INSERT INTO District_CitizenGreatPersonPoints VALUES('DISTRICT_INDUSTRIAL_ZONE', 'GREAT_PERSON_CLASS_ENGINEER', 1);
INSERT INTO District_CitizenGreatPersonPoints VALUES('DISTRICT_HANSA', 'GREAT_PERSON_CLASS_ENGINEER', 1);
INSERT INTO District_CitizenGreatPersonPoints VALUES('DISTRICT_COMMERCIAL_HUB', 'GREAT_PERSON_CLASS_MERCHANT', 1);
INSERT INTO District_CitizenGreatPersonPoints VALUES('DISTRICT_THEATER', 'GREAT_PERSON_CLASS_WRITER', 1);
INSERT INTO District_CitizenGreatPersonPoints VALUES('DISTRICT_THEATER', 'GREAT_PERSON_CLASS_ARTIST', 1);
INSERT INTO District_CitizenGreatPersonPoints VALUES('DISTRICT_THEATER', 'GREAT_PERSON_CLASS_MUSICIAN', 1);
INSERT INTO District_CitizenGreatPersonPoints VALUES('DISTRICT_ACROPOLIS', 'GREAT_PERSON_CLASS_WRITER', 1);
INSERT INTO District_CitizenGreatPersonPoints VALUES('DISTRICT_ACROPOLIS', 'GREAT_PERSON_CLASS_ARTIST', 1);
INSERT INTO District_CitizenGreatPersonPoints VALUES('DISTRICT_ACROPOLIS', 'GREAT_PERSON_CLASS_MUSICIAN', 1);

-- Increase Specialist Yields (Suboptimal)
-- Harbors/RND are now +2 gold, +1 science
-- Encampments are now +2 production, +1 culture
UPDATE District_CitizenYieldChanges SET YieldChange = 6 WHERE YieldChange = 4;
UPDATE District_CitizenYieldChanges SET YieldChange = 3 WHERE YieldChange = 2;
UPDATE District_CitizenYieldChanges SET YieldChange = 2 WHERE DistrictType = 'DISTRICT_HARBOR' And YieldType = 'YIELD_GOLD';
UPDATE District_CitizenYieldChanges SET YieldChange = 2 WHERE DistrictType = 'DISTRICT_ROYAL_NAVY_DOCKYARD' And YieldType = 'YIELD_GOLD';
UPDATE District_CitizenYieldChanges SET YieldChange = 2 WHERE DistrictType = 'DISTRICT_ENCAMPMENT' And YieldType = 'YIELD_PRODUCTION';

-- Industrial Zone gets Major Bonus for adjacent districts (Suboptimal)
UPDATE Adjacency_YieldChanges SET TilesRequired = 1 WHERE ID = 'District_Production';