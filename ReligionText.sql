INSERT OR REPLACE INTO LocalizedText (Language, Tag, Text)
VALUES
-- Reduce Defender of the Faith from +10 to +5
("en_US", "LOC_BELIEF_DEFENDER_OF_FAITH_DESCRIPTION", "Combat units gain +5 [ICON_Strength] Combat Strength when within the borders of friendly cities that follow this Religion.");