-- === Scaling Formulas === (Crystalline Cat)
UPDATE Districts SET CostProgressionModel = 'COST_PROGRESSION_GAME_PROGRESS' WHERE CostProgressionModel = 'COST_PROGRESSION_NUM_UNDER_AVG_PLUS_TECH';

UPDATE Districts SET CostProgressionParam1 = 500 WHERE CostProgressionModel = 'COST_PROGRESSION_GAME_PROGRESS';
UPDATE Projects  SET CostProgressionParam1 = 750 WHERE CostProgressionModel = 'COST_PROGRESSION_GAME_PROGRESS';
UPDATE Units     SET CostProgressionParam1 = 400 WHERE CostProgressionModel = 'COST_PROGRESSION_GAME_PROGRESS';
  -- CostProgressionParam1 was to be 1000 for districts, 1500 for projects and 800 for great people.
  
UPDATE GlobalParameters SET Value = 500 WHERE Name = 'GAME_COST_ESCALATION';

UPDATE Units SET CostProgressionParam1 = 3 WHERE UnitType = "UNIT_BUILDER";
  
-- War Weariness (Crystalline Cat)
-- UPDATE Eras SET WarmongerPoints = 0;