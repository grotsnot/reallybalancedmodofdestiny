=== Feature List ===

Changes that start with a '-' are active in the mod.  
Changes that start with a '#' are coded but inactive (commented out).

<- General Gameplay ->

- Scaling formulas were changes to reduce cost increases (Crystalline Cat)
# Era scaling factor for war weariness set to 0 (Crystalline Cat)

<- Buildings ->

- Costs reduced per Sulla's suggestions (Crystalline Cat)
- Research Lab provides +6 science (Crystalline Cat)
- Bank provides +6 gold (Crystalline Cat)
- Stock Exchange provides +10 gold (Crystalline Cat)
- Granaries and Sewers provide 3 housing instead of 2 (Crystalline Cat)
- All buildings provide a +25% bonus to the corresponding yield type in 
  Campus, Theater, Holy Site, Industrial Zone and Commercial Hub districts. 
  (Suboptimal)
- District buildings have 2 citizen slots (Suboptimal)
- Zoo moved to Colonialism per Bacchus' suggestion (Suboptimal)
- Encampment buildings reduce war weariness accumulation (Suboptimal):
  > Barracks/Stables: -2% 
  > Armory: -3%
  > Military Academy: -5%
 
<- Cities ->

- City housing minimums increased (Suboptimal)
  > No Water: 3
  > Coastal: 4
  > Fresh Water: 6
- City growth penalty due to housing changed (Crystalline Cat)
  > Growth -50% at housing limit
  > Growth -75% at one over housing limit
  > Growth halted at five over housing limit
- City population growth and cultural expansion rates changed per Sulla's
  suggestion (Crystalline Cat)  Updated to Hans Lemurson's suggestion of 0 
  for the city growth exponent and 1.0 for the cultural growth exponent.
- Amenities spread to 5 cities (7 cities for Aztecs) (Crystalline Cat)

<- Districts ->

- All specialists generate 1 Great Person Point per turn, Theaters 1 GPP for each type (Suboptimal)
- All specialist yields were increased (Suboptimal):
  > Non-gold: +3 (from +2)
  > Gold: +6 (from +3)
  > Harbors: specialists provide 2 gold and 1 science
  > Encampments: specialists provide 2 production and 1 culture
- Industrial Zone gets +1 adjacency bonus per district (Suboptimal)

<- Tile Improvements ->

- Forest planting moved to Recorded History (Crystalline Cat)
- Camps provide 1 gold and 1 food (Crystalline Cat)
- Camps provide +1 gold instead of +1 food at Mercantilism (Crystalline Cat)
- Road improvements moved up in era per Sulla's suggestions (Crystalline Cat)
  > Ancient Era: roads provide bridges
  > Medieval Era: roads cost 0.75MP to use
  > Industrial roads moved to Renaissance Era
  > Modern roads moved to Industrial Era
# Farms on grasslands and plains provide +2 food (Suboptimal)
# Farms on rice and floodplains provide +1 food (Suboptimal)
# Farms provide +1 housing (Suboptimal)

<- Policies ->

+ Military +

- Professional Army discount is 33% (Crystalline Cat)
- Naval production policy bonus is 50% (Crystalline Cat)
- Limes production bonus is 50% (Crystalline Cat)
- Levee en Masse moved to Nationalism (Crystalline Cat)
- International Waters moved to Mobilization (Crystalline Cat)
- Agoge obsoletes when Grande Armee is unlocked (Crystalline Cat)
- Chivalry obsoletes when Military First is unlocked (Crystalline Cat)
- Feudal Contract obsoletes when Military First is unlocked (Crystalline Cat)
- Press Ganges obsoletes when Military First is unlocked (Crystalline Cat)
- Bastions, Discipline, Grande Armee, Limes and Retainers do not obsolete 
  (Crystalline Cat)
  
+ Economic + 

- Expropriation and Land Surveyors gold discount is 33% (Crystalline Cat)
- Ilkum and Public Works production discount is 50% (Crystalline Cat)
- Ilkum Obsoletes when Public Works is unlocked (Crystalline Cat)
- Corvee, Gothic Architecture and Urban Planning do not obsolete
  (Crystalline Cat)
# Rationalism, Free Market, Simultaneum and Grand Opera provide +100% yield 
  for district buildings and +25% city yield for the corresponding yield type.
<- Religion ->

- Defender of the Faith provides a +5 bonus (Crystalline Cat)

<- Units ->

- Unit costs reduced per Sulla's suggestions (Crystalline Cat)
- Unit upgrades now cost 2x production cost in gold (Crystalline Cat)
- Formation of Corps/Fleets moved to Guilds (Crystalline Cat)
- Formation of Armies/Armadas moved to Nationalism (Crystalline Cat)
- Machine Gun range is 2 (Crystalline Cat)

<- Wonders ->

- Venetian Arsenal now only works for the city it's built in (BrickAstley)

